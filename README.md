# Nightnet Website

This project consisted of a set of static web pages to showcase basic HTML and CSS skills for West Governor's University's Web Development Fundamentals class.

## Requirements

School policies do not allow me to upload the rubric, scenario or specific project requirements here, but I will paraphrase as best I can to relay the information. Here are the requirments for the site:
- Create at least four pages.
- Properly use these HTML semantic elements: header, footer, main, article, section, aside, nav, and footer.
- Use an external stylesheet to provide a consitent look and feel across all pages.
- Include at least one element with the _float_ property.
- Inlcude at least one element with _absolute_ positioning.
- Include at least one element with _relative_ positioning.
- Use the `:hover` pseudo-class.
- Include at least one image.
- Create a link that opens an external page in a new tab/window.
- Include a table, unordered list, and ordered list.
- The site must pass validation through the W3C's HTML validator at the time of submission.


## Development

Developing static HTML pages with CSS stylesheets is relatively easy. This project took me only a couple days and I didn't run into any issues.

## Shortcomings

In Edge, the footer doesn't center properly. Also the links in the footer blend into the background. If I were to revise this, I would either change the color of the links in the stylesheet, or make the footer have a different background color.